describe('ventana principal', () => {
    it('Llenando el formulario', () => {
        cy.visit('http://localhost:4200');
        cy.contains('WishList (form-destino-viaje)');
        cy.get("form");

        cy.get('input[id="nombre"]')
            .type("Bangkok")
            .should("have.value", "Bangkok");

        cy.get('input[id="imagenUrl"]')
            .type("Bangkok URL")
            .should("have.value", "Bangkok URL");

        cy.get("form").submit();
    });
});