import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  @Output() onItemDeleted: EventEmitter<DestinoViaje>;

  titulo: 'Lista de destinos';
  updates: string[];
  all: any;

  constructor( private destinosApiClient: DestinosApiClient,
               private store: Store<AppState> ) {
     this.onItemAdded = new EventEmitter();
     this.onItemDeleted = new EventEmitter();

     this.store.select( state => state.destinos.favorito)
     .subscribe(d => {
        if (d != null) {
          this.updates.push('se ha elegido a ' + d.nombre );
        }
     });
     store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  elegido( e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }

  getAll() {

  }

  public eliminarItem(d: DestinoViaje) {
    if (window.confirm('¿Desea eliminar el destino ?')) {
      this.destinosApiClient.eliminar(d);
      this.onItemDeleted.emit(d);
    }
    return false;
  }

}
