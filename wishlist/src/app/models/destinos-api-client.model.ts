import { Injectable, forwardRef, Inject } from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import {
  NuevoDestinoAction,
  ElegidoFavoritoAction,
  EliminarDestinoAction
} from './destinos-viajes-state.model';
import { AppState, APP_CONFIG, AppConfig, MyDatabase, db } from '../app.module';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[] = [];

    constructor(private store: Store<AppState>,
                @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
                private http: HttpClient) {
      this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log(data);
        this.destinos = data.items;
      });
      this.store
      .subscribe((data) => {
        console.log(data);
      });
    }
    add(d: DestinoViaje) {
      const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
      const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
      this.http.request(req).subscribe((data: HttpResponse<{}>) => {
        if (data.status === 200) {
          this.store.dispatch(new NuevoDestinoAction(d));
          const myDb = db;
          myDb.destinos.add(d);
          myDb.destinos.toArray().then(destinos => console.log(destinos));
        }
      });
    }


    elegir(d: DestinoViaje) {
      console.log('kgc. Elegir destino-api-client');
      this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    eliminar(d: DestinoViaje) {
      console.log('kgc. Eliminar destino-api-client');
      this.store.dispatch(new EliminarDestinoAction(d));
    }

    getById(id: string): DestinoViaje {
      return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
    }

    getAll(): DestinoViaje[] {
      return this.destinos;
    }
}
