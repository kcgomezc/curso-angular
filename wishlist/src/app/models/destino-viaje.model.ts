import {v4 as uuid} from 'uuid';
export class DestinoViaje {

    // nombre: string;
    // imagenurl: string;

    // constructor(n, u) {
    //     this.nombre = n;
    //     this.imagenurl = u;
    // }

    selected: boolean;
    servicios: string[];
    id = uuid();
    public votes = 0;

    constructor(public nombre: string, public imagenurl: string ) {
        this.servicios = ['Pileta', 'Desayuno'];
    }

    setSelected(s: boolean) {
        this.selected = s;
      }
      isSelected() {
        return this.selected;
      }
      voteUp(): any {
        this.votes++;
      }
      voteDown(): any {
        this.votes--;
      }

      voteReset(): any {
        this.votes = 10;
      }
}
